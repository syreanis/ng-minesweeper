import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { StartingComponent } from "./starting/starting.component";
import { EndingComponent } from "./ending/ending.component";
import { GameComponent } from "./game/game.component";

const routes: Routes = [
  { path: "", component: StartingComponent },
  { path: "game", component: GameComponent },
  { path: "ending", component: EndingComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
