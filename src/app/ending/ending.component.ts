import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { Router } from "@angular/router";
import { getGameState, changeGameState } from "../reducers/game";

@Component({
  selector: "app-ending",
  templateUrl: "./ending.component.html",
  styleUrls: ["./ending.component.css"]
})
export class EndingComponent implements OnInit {
  gameState$: Observable<number>;
  icon: string;

  constructor(private store: Store<any>, private router: Router) {
    this.gameState$ = this.store.select(getGameState);
    this.gameState$.subscribe(
      gameState => (this.icon = gameState === 3 ? "mood" : "mood_bad")
    );
  }

  ngOnInit() {}

  startGame() {
    this.store.dispatch(changeGameState(0));
    this.router.navigate(["/game"]);
  }
}
