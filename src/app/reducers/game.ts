import { Action } from "@ngrx/store";

export enum GameStateEnum {
  NotStarted,
  Started,
  Losing,
  Winning,
}

enum GameActionTypes {
  SetConfig = "SetConfig",
  ChangeGameState = "ChangeGameState"
}

export interface GameConfig {
  rows: number;
  cols: number;
  mines: number;
}

class SetConfigAction implements Action {
  type: GameActionTypes.SetConfig;
  payload: GameConfig;
}

class ChangeGameStateAction implements Action {
  type: GameActionTypes.ChangeGameState;
  payload: GameStateEnum;
}

type GameActionsUnion = SetConfigAction | ChangeGameStateAction;

export interface gameReducerState {
  state: GameStateEnum;
  config: GameConfig;
}

export const initialState: gameReducerState = {
  state: GameStateEnum.NotStarted,
  config: {
    rows: 4,
    cols: 4,
    mines: 4
  }
};

export function setConfig(config: GameConfig): GameActionsUnion {
  return {
    type: GameActionTypes.SetConfig,
    payload: config
  };
}

export function changeGameState(state: GameStateEnum): GameActionsUnion {
  return {
    type: GameActionTypes.ChangeGameState,
    payload: state
  };
}

export function gameReducer(
  state: gameReducerState = initialState,
  action: GameActionsUnion
): gameReducerState {
  switch (action.type) {
    case GameActionTypes.ChangeGameState:
      return {
        ...state,
        state: action.payload
      };
    case GameActionTypes.SetConfig:
      return {
        ...state,
        config: action.payload
      };
    default:
      return state;
  }
}

export const getConfig = (state: any) => state.game.config;

export const getGameState = (state: any) => state.game.state;
