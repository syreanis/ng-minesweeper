import { MinesweeperService } from "./../minesweeper.service";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Store } from "@ngrx/store";
import {
  getConfig,
  getGameState,
  changeGameState,
  GameConfig
} from "../reducers/game";
import { Observable } from "rxjs";

@Component({
  selector: "app-game",
  templateUrl: "./game.component.html",
  styleUrls: ["./game.component.css"]
})
export class GameComponent implements OnInit {
  tiles = [];
  config$: Observable<GameConfig>;
  gameState$: Observable<string>;
  cols: number;
  constructor(
    private minesweeperService: MinesweeperService,
    private store: Store<any>,
    private router: Router
  ) {
    this.config$ = this.store.select(getConfig);
    this.gameState$ = this.store.select(getGameState);
    this.config$.subscribe(config => {
      this.cols = config.cols;
      this.minesweeperService.generateBoard(config);
    });
    this.gameState$.subscribe(gameState => this.checkGameState(gameState));
  }

  ngOnInit() {
    this.tiles = this.minesweeperService.getGrid();
  }

  checkGameState(gameState) {
    console.log(gameState);
    switch (gameState) {
      case 2:
      case 3:
        this.router.navigate(["/ending"]);
        break;
      default:
        break;
    }
  }

  openCell(x, y) {
    this.minesweeperService.openCell(x, y);
    this.tiles = this.minesweeperService.getGrid();
    const boardState = this.minesweeperService.checkBoardState();
    this.store.dispatch(changeGameState(parseInt(boardState, 10)));
  }

  cycleCellFlag(x, y) {
    this.minesweeperService.cycleCellFlag(x, y);
    this.tiles = this.minesweeperService.getGrid();
    const boardState = this.minesweeperService.checkBoardState();
    this.store.dispatch(changeGameState(parseInt(boardState, 10)));
  }
}
