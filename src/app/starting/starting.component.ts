import { Component, OnInit } from "@angular/core";
import { GameConfig, setConfig, changeGameState } from "../reducers/game";
import { Store } from "@ngrx/store";
import { Router } from "@angular/router";

@Component({
  selector: "app-starting",
  templateUrl: "./starting.component.html",
  styleUrls: ["./starting.component.css"]
})
export class StartingComponent implements OnInit {
  startConfigForm: GameConfig = {
    rows: 4,
    cols: 4,
    mines: 4
  };
  constructor(private store: Store<any>, private router: Router) {}
  ngOnInit() {}

  startGame(event) {
    event.preventDefault();
    this.store.dispatch(setConfig(this.startConfigForm));
    this.store.dispatch(changeGameState(0));
    this.router.navigate(["/game"]);
  }
}
