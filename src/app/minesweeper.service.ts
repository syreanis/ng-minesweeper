import { Injectable } from "@angular/core";
import minesweeper from "minesweeper";

@Injectable({
  providedIn: "root"
})
export class MinesweeperService {
  board: any;
  constructor() {}

  generateBoard(config): void {
    const mineArray = minesweeper.generateMineArray(config);

    this.board = new minesweeper.Board(mineArray);
  }

  openCell(x: number, y: number): void {
    this.board.openCell(x, y);
  }

  cycleCellFlag(x: number, y: number): void {
    this.board.cycleCellFlag(x, y);
  }

  checkBoardState(): string {
    return this.board.state();
  }

  getGrid() {
    const grid = this.board.grid();

    return grid.reduce((acc, val) => acc.concat(val), []).map(cell => {
      let state;
      if (cell.state === minesweeper.CellStateEnum.CLOSED) {
        if (cell.flag === minesweeper.CellFlagEnum.NONE) {
          state = " ";
        } else if (cell.flag === minesweeper.CellFlagEnum.EXCLAMATION) {
          state = "!";
        } else if (cell.flag === minesweeper.CellFlagEnum.QUESTION) {
          state = "?";
        }
      } else if (cell.state === minesweeper.CellStateEnum.OPEN) {
        if (cell.isMine) {
          state = "*";
        } else {
          state = cell.numAdjacentMines;
        }
      }
      return {
        state,
        x: cell.x,
        y: cell.y
      };
    });
  }
}
