import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";

import {
  MatToolbarModule,
  MatCardModule,
  MatButtonModule,
  MatSliderModule,
  MatFormFieldModule,
  MatInputModule,
  MatIconModule,
  MatGridListModule
} from "@angular/material";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { StartingComponent } from "./starting/starting.component";
import { EndingComponent } from "./ending/ending.component";
import { FormsModule } from "@angular/forms";
import { GameComponent } from "./game/game.component";
import { GameTileComponent } from "./game-tile/game-tile.component";
import { ServiceWorkerModule } from "@angular/service-worker";
import { StoreModule } from "@ngrx/store";
import { StoreRouterConnectingModule } from "@ngrx/router-store";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { environment } from "../environments/environment";
import { reducers, metaReducers } from "./reducers";

@NgModule({
  declarations: [
    AppComponent,
    StartingComponent,
    EndingComponent,
    GameComponent,
    GameTileComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MatToolbarModule,
    MatCardModule,
    MatButtonModule,
    MatSliderModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatGridListModule,
    ServiceWorkerModule.register("/ngsw-worker.js", {
      enabled: environment.production
    }),
    StoreModule.forRoot(reducers, { metaReducers }),
    StoreRouterConnectingModule.forRoot({
      stateKey: "router"
    }),
    StoreDevtoolsModule.instrument({
      name: "NgRx Book Store DevTools",
      logOnly: environment.production
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
